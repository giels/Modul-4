/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anggi Saputra
 */
import java.awt.*;

public class FlowLayoutDemo extends Frame{
   public  FlowLayoutDemo(){
       
   }
   public static void main(String args[]){
       FlowLayoutDemo fld = new FlowLayoutDemo();
       fld.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 10));
       fld.add(new Button("Press Me"));
       fld.add(new Button("Don't press Me"));
       fld.setSize(100, 100);
       fld.setVisible(true);
   }
}
