/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anggi Saputra
 */
import java.awt.*; 

public class FrameExample {
    private Frame f;
    
    public FrameExample(){
        f = new Frame("Hello Out There");
    }
    public void launchFrame (){
        f.setSize(170, 170);
        f.setBackground(Color.blue);
        f.setVisible(true);
    }
    public static void main(String[] args){
        FrameExample guiWindow = new FrameExample();
        guiWindow.launchFrame();
    }
}
