/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anggi Saputra
 */
import java.awt.*;

public class FrameWithPanel {
    private Frame f;
    
    public FrameWithPanel(String title){
        f = new Frame(title);
    }
    
    public void launchFrame(){
        f.setSize(200, 200);
        f.setBackground(Color.blue);
        f.setLayout(null);
        
        Panel pan = new Panel();
        pan.setSize(100, 100);
        pan.setBackground(Color.yellow);
        
        Panel aa= new Panel();
       
        aa.setSize(50, 50);
        aa.setBackground(Color.green);
        
        f.add(aa);
        
        f.add(pan);
        
        
        f.setVisible(true);
    }
    
    public static void main(String args[]){
        FrameWithPanel guiWindow = new FrameWithPanel("Frame With Panel");
        guiWindow.launchFrame();
    }
}
