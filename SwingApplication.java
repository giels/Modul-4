import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class SwingApplication extends Frame implements ActionListener{

	int numClicks = 0;
	Label label1;
	JButton button1;
	
	public SwingApplication(){
		this.button1 = new JButton("im a swing button");
		this.button1.addActionListener(this);
		
		this.label1 = new Label("label prefix :"+numClicks);
		
		this.add(label1, BorderLayout.CENTER);
                this.add(button1, BorderLayout.NORTH);
	}
	
        
        @Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
            numClicks ++;
            label1.setText("label prefix = "+numClicks);
	}
        
        public static void main(String[] args){
            SwingApplication frm = new SwingApplication();
            frm.setSize(100, 100);
            frm.setVisible(true);
        }
        
        
      
}